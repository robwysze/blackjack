from game import Game


def main():
    game = Game()
    again = game.run().lower()

    if again == 'yes':
        main()
        return 0


if __name__ == '__main__':
    main()
