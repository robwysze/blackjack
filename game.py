from deck import Deck
from hand import Hand
from chips import Chips


class Game:
    def __init__(self):
        pass

    def run(self):
        deck = Deck()
        deck.shuffle()
        player = [Hand('Croupier'), Hand()]
        done = False

        player[0].add_card(deck.deal())
        for i in range(2):
            player[1].add_card(deck.deal())

        print(str(player[0]) + '\t| Total value: {}'.format(player[0].value))
        print(str(player[1]) + '\t| Total value: {}'.format(player[1].value))

        chips = Chips()

        while not done:
            if self.check_value(player[1].value):
                if self.check_aces(player[1].aces):
                    player[1].adjust_for_ace()
                    move = self.next_move()
                    if move == 1:
                        player[1].add_card(deck.deal())
                    else:
                        done = True
                else:
                    done = True
            else:
                move = self.next_move()
                if move == 1:
                    player[1].add_card(deck.deal())
                else:
                    done = True
            print(str(player[0]) + '\t| Total value: {}'.format(player[0].value))
            print(str(player[1]) + '\t| Total value: {}'.format(player[1].value))

        done = False
        if not self.check_value(player[1].value):
            print('\n\n')
            print(str(player[0]) + '\t| Total value: {}'.format(player[0].value))
            print(str(player[1]) + '\t| Total value: {}'.format(player[1].value))
            while not done:
                if player[0].value < 17:
                    player[0].add_card(deck.deal())
                else:
                    if self.check_value(player[0].value):
                        if self.check_aces(player[0].aces):
                            player[0].adjust_for_ace()
                            player[0].add_card(deck.deal())
                        else:
                            done = True
                    else:
                        done = True
                print(str(player[0]) + '\t| Total value: {}'.format(player[0].value))
                print(str(player[1]) + '\t| Total value: {}'.format(player[1].value))
        else:
            print(player[1].name + ' lost')
            return self.play_again()

        if player[0].value > 21:
            print(player[1].name + ' won')
        else:
            if 22 > player[1].value >= player[0].value:
                print(player[1].name + ' won')
            else:
                print(player[1].name + ' lost')

        return self.play_again()

    @staticmethod
    def check_value(value):
        if value > 21:
            return True
        else:
            return False

    @staticmethod
    def check_aces(aces):
        if aces > 0:
            return True
        else:
            return False

    @staticmethod
    def next_move():
        print('Option:\t1.Hit\t2.Stand\t')
        return int(input('Enter your move: '))

    @staticmethod
    def play_again():
        return input('Play again [yes/no]:\t')